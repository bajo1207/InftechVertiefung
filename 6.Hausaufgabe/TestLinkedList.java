public class TestLinkedList{

    // Bekommt eine LinkedList uebergeben und printet alle ELemente auf die Konsole
    public static void printList(LinkedList list) {
    	// TODO
        int size = list.size();
        for(int i = 0; i < size; i++){ //Goes through linked list
            System.out.println(i+". Element im liste: ");
            System.out.println(list.get(i).toString()); //Gets Film of given ListElem and calls the toString method
            System.out.println();
        }
    }

    // Bekommt eine LinkedList und einen Index uebergeben und entfernt (und printet)
    // aus der LinkedList das Element an Index i. Gibt ggf. eine Fehlermeldung aus.
    public static void removeAtIndex(LinkedList list, int index) {
        // TODO
        if(index < list.size() && index >= 0) list.remove(index); //removes ListElem at given index
        else System.out.println("Index nicht gefunden");
    }


    // Testet die statischen Methoden sowie die Listenmethoden direkt
    public static void main(String[] args){
        LinkedList link = new LinkedList();
        link.addHead(new Film("Heyo", 10, 22, "Coole film", "1982-11-22")); //addHead
        link.addHead(new Film("AndereFilm", 11, 31, "comment", "1888-01-29")); //addHead
        link.addHead(new Film("Menn som stirrer på geiter", 0, 94, "Goats Goats Goats", "2009-11-06")); //addHead
        link.addHead(new Film("The Room", 54, 99, "Best movie ever", "2003-06-27")); //addhead
        printList(link); //get(int index)
        System.out.println();
        System.out.println();
        removeAtIndex(link, 0); //Remove at index/remove(int index)
        removeAtIndex(link, 1);
        link.addTail(new Film("Gordon Ramsay gone bad", 10, 300, "Nothing", "2020-06-10")); //addTail
        System.out.println("Film @ Tail: "+link.getTail().toString()); //getTail
        Film mov = new Film("Just another movie", 0, 3, "Something", "1000-03-17"); 
        link.add(mov, 1); //add movie at given index
        link.removeHead(); //removeHead
        link.removeTail(); //removeTail
        System.out.println();
        System.out.println("Anzahl listElems: "+link.size()); //test size()
        System.out.println();
        System.out.println("The list is empty = "+link.isEmpty()); //test isEmpty()
        System.out.println();
        System.out.println("Just another movie is at index: "+link.find(mov)); //test find()
        System.out.println();
        System.out.println();
        printList(link);
    }

}
