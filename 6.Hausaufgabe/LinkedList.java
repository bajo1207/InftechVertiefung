public class LinkedList {
    
    //Attribute
    private ListElem head;
    private ListElem tail;

    private class ListElem {
        Film data;
        ListElem next;
        
  
        ListElem(Film data) {
            this.data = data;
            this.next = null;
        }

        ListElem(Film data, ListElem next) {
            this.data = data;
            this.next = next;
        }
    }

    //Konstruktor erzeugt eine leere Liste
    public LinkedList() {  
        this.head = null;
        this.tail = null;
    }

    // Methoden
    public Film getHead(){
        if(!isEmpty()) return this.head.data; //Returns content of head
        return null;
    }
    
    public Film getTail(){
        if(!isEmpty()) return this.tail.data; //Returns content of tail
        return null;
    }

    public Film get(int pos){ //Gets content of a given element
        ListElem ret = getElem(pos); //Failuretesting is in getElem
        return ret.data;
    }

    public void addHead(Film val){ //Adds new head
        if(isEmpty()){ //If list empty, new head is both head and tail
            this.head = new ListElem(val);
            this.tail = this.head;
        }
        else{
            ListElem temp = new ListElem(val, this.head); //new head with current head as next.
            this.head = temp;
        }
    }

    public void addTail(Film val){ //Adds new tail
        if(isEmpty()){ //If list empty, new tail is both head and tail
            this.head = new ListElem(val);
            this.tail = this.head;
        }
        else{
            this.tail.next = new ListElem(val);
            this.tail = this.tail.next; //tail is new element
        }
    }

    public void add(Film val, int pos){
        if(pos >= 0){
            if(pos == 0 || isEmpty()) addHead(val); //When index 0 addHead
            else if(pos >= size()-1) addTail(val); //When index >= tail addTail
            else{
                ListElem pre = getElem(pos-1); //gets element of pos-1
                ListElem temp = new ListElem(val, pre.next);//Inserts new element
                pre.next = temp; //
            }

        }
    }

    public Film removeHead(){ //removes head
        if(isEmpty()) return null; //If empty return null
        Film ret = this.head.data;
        if(this.head == this.tail){ //If only one element, removes that element
            this.head = null;
            this.tail = null;

        }
        else this.head = this.head.next; //Else removes head
        return ret;
    }

    public Film removeTail(){ //Removes tail
        if(isEmpty()) return null; //If empty return null
        Film ret = this.tail.data;
        ListElem temp = getElem(size()-2);
        
        if(this.head == this.tail){ // If only one element, removes that element
            this.head = null;
            this.tail = null;
        }
        else{
            this.tail = temp; //Else removes tail
            temp.next = null;
        }
        return ret;
    }

    public Film remove(int pos){ //Removes element at given index
         if(pos < 0 || pos >= size() || isEmpty()) return null; //If no elements, or index out of bounds, return null
         if(pos == 0) return removeHead(); // If index = head removeHead
         if(pos == size()-1) return removeTail(); //If index = tail removeTail
         ListElem pre = getElem(pos-1); 
         Film ret = pre.next.data; // saves filmelement
         pre.next = pre.next.next; //removes element
         return ret;
    }

    public boolean isEmpty(){ //Checks if List is empty
        if(head == null) return true;
        return false;
    }

    public int size(){ //Checks size of list
        ListElem temp = this.head;
        if(isEmpty()) return 0;
        int i = 1;
        while(temp.next != null){
            temp = temp.next;
            i++;
        }
        return i;
    }

    private ListElem getElem(int idx){ //Helpmethod, to reduce repetition. Returns element of given position.
        if(idx < 0) return null;
        
        ListElem current = this.head;
        int i;
        for(i = 0; i < idx && current.next != null; i++){
            current = current.next;
        }
        if(i != idx) return null;
        return current;
    }

    public int find(Film obj){ //Finds a movie in Linked list.
        ListElem curr = this.head;
        int idx = -1;
        for(int i = 0; i < size(); i++){
            if(curr.data == obj){
                idx = i;
                break;
            }
            curr = curr.next;
        }
        return idx;
    }

    // TODO
    
}
