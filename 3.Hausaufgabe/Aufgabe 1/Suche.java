public class Suche  {    
    // Suchmethode
    // TODO
    public static int binaereSuche(Film suchObjekt, Film[] array){ //Binaere suche nach bestimmte film
    	int resindex = -1;
    	int start = -1;
    	int end = array.length;
    	int mitte;
    	while(resindex == -1 && ((end-start)/2) != 0){
    		mitte = ((end+start)/2); //Findet die mittlere element zwischen start und ende
    		if(suchObjekt.compareTo(array[mitte]) == 0){ //Element ist gefunden, resindex = mitte
    			resindex = mitte;
    		}
    		else if (suchObjekt.compareTo(array[mitte]) > 0){ //wenn erscheinungsdatum später ist dann ist der start die neue mitte
    			start = mitte;
    		}
    		else{
    			end = mitte; //wenn erscheinungsdatum früher ist dann ist der end die neue mitte
    		}
    	}
    	return resindex; 

    }
     


}
