public class Film  {
    
    // Attribute
    public String titel;
    public double preis; // in EUR
    public int    laenge; // in min
    public String beschreibung;
    public String erscheinungsdatum; // ISO-8601 (JJJJ-MM-TT)
    
    // Konstruktor
    public Film(String titel, double preis, int laenge, String beschreibung, String erscheinungsdatum) {
        this.titel = titel;
        this.preis = preis;
        this.laenge = laenge;
        this.beschreibung = beschreibung;
        this.erscheinungsdatum = erscheinungsdatum;
    }
    
    // Methoden
    // TODO
    public String toString(){ //Gibt alle attribute als string aus.
        return "Titel: "+titel+"\n"+"Preis: "+preis+"\n"+"Laenge: "+laenge+"\n"
        +"Beschreibung: "+beschreibung+"\n"+"Erscheinungsdatum: "+erscheinungsdatum;
    }

    public int compareTo(Film o){
        return this.erscheinungsdatum.compareTo(o.erscheinungsdatum); //string.compare to funktioniert solange die erscheinungsdatum in ISO-8601 ist.
    }

}
