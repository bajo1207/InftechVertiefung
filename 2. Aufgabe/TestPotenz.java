public class TestPotenz{
	public static void main(String[] args) {
		
		System.out.println("Fasit:");
		System.out.println("2.2^10 = "+Math.pow(2.2,10)+"\n3^0 = "+Math.pow(3,0)); //Math.pow aufrufen
		System.out.println();
		
		System.out.println("Iterativ:");
		System.out.println("2.2^10 = "+Potenz.potenzIterativ(2.2,10)+"\n3^0 = "+Potenz.potenzIterativ(3,0)); //Ausrechnen iterativ
		System.out.println();
		
		System.out.println("Rekursiv:");
		Potenz rek = new Potenz(); //Objekt erzeugen
		System.out.println("2.2^10 = "+rek.potenzRekursiv(2.2,10)+"\n3^0 = "+rek.potenzRekursiv(3,0)); //Ausrechnen Rekursiv
	}
}