public class Potenz{
	public static double potenzIterativ(double a, int n){ //Ruckgabewert double
		if(a >= 0 && n >= 0){
			double pot = 1;
			for(int i = 0; i < n; i++){
				pot *= a;
			}
			return pot;
		}
		else{
			return -1;
		}
	}

	public double potenzRekursiv(double a, int n){ //Ruckgabewert double
		if(a < 0 && n < 0){
			return -1;
		}
		if(n == 0){ //Abbruchbedingung
			return 1; 
		}

		else{
			return potenzRekursiv(a, n-1) * a; //Rekursive aufruf
		}
	}
}
