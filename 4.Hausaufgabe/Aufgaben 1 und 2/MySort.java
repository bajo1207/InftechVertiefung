public class MySort{
	public static void mySort(Comparable[] f){
		
		int compareToCount = 0; 
		int swapCount = 0;
		int sortLinePos = 0; //Ist die teilung pos zwischen sortiert und unsortiert.
		
		for(int i = 0; i < f.length; i++){
			int smalElem = sortLinePos; //SmalElem ist die anfang des nicht sortiertes Array.
			for(int j = smalElem + 1; j < f.length; j++){ //Geht von anfang des erstes arrays zu ende des Arrays. 
				if(f[smalElem].compareTo(f[j]) > 0) smalElem = j; 
				compareToCount++; //CompareTo wir jedes mal aufgerufen. 
			}
			if(f[sortLinePos] != f[smalElem]){ //Tauscht die elemente wenn es ein kleinere element gibt.
				Comparable temp = f[sortLinePos];
				f[sortLinePos] = f[smalElem];
				f[smalElem] = temp;
				swapCount++;
			}
			sortLinePos++;
		}
		System.out.println("Swap Count: "+swapCount);
		System.out.println("CompareTo Count: "+compareToCount);
		System.out.println();
	}
}	