public class SortierenQuicksort{
	
	static int compareCount = 0;
	static int swichCount = 0;
	
	public static void quickSort(Comparable[] f){
		int pivot = (int)(Math.random() * (f.length-1)); //Generiert eine zufällige nummer zwischen 0 und die Laenge des arrays
		if(pivot != f.length-1){ //Setzt die zufällige pivot am ende des arrays
			Comparable temp = f[pivot];
			f[pivot] = f[f.length-1];
			f[f.length-1] = temp;
			pivot = f.length-1;
			swichCount++;
		}

		int i; //Laufindex
		int g = 0; //Grenzindex

		for(i = 0; i < f.length-1; i++){ //Wenn array[i] grösser array[pivot] kein tausch, i++
			if(f[i].compareTo(f[pivot]) < 0){ //wenn array[i] kleiner array[Pivot] Tausche array[i] und array[g], i++, g++
				Comparable temp = f[i];
				f[i] = f[g];
				f[g] = temp;
				swichCount++; //Zaelt anzahl tausch
				g++;
			}
			compareCount++;
		}

		Comparable temp = f[pivot]; //Setst die pivot im richtigen Position im Array
		f[pivot] = f[g];
		f[g] = temp;
		pivot = g;
		swichCount++;

		Comparable[] smal = new Comparable[pivot]; //Teil links vom pivot wird ein neue array smal
		for(int x = 0; x < pivot; x++){
			smal[x] = f[x];
		}
		Comparable[] large = new Comparable[f.length-pivot-1]; //Teil rechts vom pivot wird ein neue array large
		for(int x = 0; x < large.length; x++){
			large[x] = f[x+pivot+1];
		}
		
		if(smal.length > 1) quickSort(smal); //Rekursive aufruf von smal
		if(large.length > 1) quickSort(large); //Rekursive aufruf von large
		
		for(int x = 0; x < pivot; x++){ //Smal wird zu f zugefugt
			f[x] = smal[x];
		}
		for(int x = 0; x < large.length; x++){ //Large wird zu f zugefugt
			f[x+pivot+1] = large[x];
		}


		if(smal.length <= 1 && large.length <= 1){
			System.out.println("compareCount: "+compareCount);
			System.out.println("swichCount: "+swichCount);
			System.out.println();
		}
	}
}	