public class TestStack{
	public static void main(String[] args){
		Stack st = new Stack();

		st.push(new Film("Heyo", 10, 22, "Coole film", "1982-11-22")); 
        st.push(new Film("AndereFilm", 11, 31, "comment", "1888-01-29")); 
        st.push(new Film("Menn som stirrer på geiter", 0, 94, "Goats Goats Goats", "2009-11-06")); 
        st.push(new Film("The Room", 54, 99, "Best movie ever", "2003-06-27"));
        st.push(new Film("Gordon Ramsay gone bad", 10, 300, "Nothing", "2020-06-10"));

        System.out.println("Oberste Element = "+st.top().toString());

        for(int i = 0; i < 6; i++) st.pop();

        System.out.println("Oberste Element = "+st.top());






	}
}