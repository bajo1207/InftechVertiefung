	public class Stack {
	private Film[] arr;
	private int curentArrSize;
	private int countElem;

	public Stack(){
		arr = new Film[4];
		curentArrSize = 4;
		countElem = 0;
	}

	public void push(Film obj){
		if(curentArrSize == countElem){
			Film[] tmp = new Film[2*curentArrSize];
			for(int i = 0; i < curentArrSize; i++) tmp[i] = this.arr[i];
			curentArrSize *= 2;
			arr = tmp;
		}
		this.arr[countElem] = obj;
		countElem++;
	}

	public Film pop(){
		if(countElem == 0) return null;
		Film tmp = arr[countElem];
		arr[countElem] = null;
		countElem--;
		return tmp;
	}

	public Film top(){
		if(countElem == 0) return null;
		return arr[countElem-1];
	}

}