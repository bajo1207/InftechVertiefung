public class TestBaum{
	public static void main(String[] args) {
		Baum b = new Baum(new String[] {"O","G","N","C","F","J","M","A","B","D","E","H","I","K","L"});
		System.out.println("In-Order:");
		b.printInorder();
		System.out.println();
		System.out.println("Pre-Order:");
		b.printPreorder();
		System.out.println();
		System.out.println("Pos-Order:");
		b.printPostorder();
	}
}