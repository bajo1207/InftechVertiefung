public class Baum{
	public String[] knoten;
	public Baum(String[] knoten){
		this.knoten = knoten;
	}

	public void printInorder(){
		printInorder(0);
		System.out.println();
	}
	private void printInorder(int i){
		if(i >= knoten.length) return;
		printInorder(getLeftChild(i));
		System.out.print(knoten[i]+" ");
		printInorder(getRightChild(i));
	}

	public void printPreorder(){
		printPreorder(0);
		System.out.println();
	}

	private void printPreorder(int i){
		if(i >= knoten.length) return;
		System.out.print(knoten[i]+" ");
		printPreorder(getLeftChild(i));
		printPreorder(getRightChild(i));

	}

	public void printPostorder(){
		printPostorder(0);
		System.out.println();
	}

	public void printPostorder(int i){
		if(i >= knoten.length) return;
		printPostorder(getLeftChild(i));
		printPostorder(getRightChild(i));
		System.out.print(knoten[i]+" ");
	}

	private int getLeftChild(int index){
		return 2 * index + 1;
	}

	private int getRightChild(int index){
		return 2 * index + 2;
	}
}