import java.util.LinkedList;
import java.util.NoSuchElementException;
public class Queue<T>{
	private LinkedList<T> list;

	public Queue(){
		list = new LinkedList<>();
	}

	public void enqueue(T val){
		list.add(val);
	}

	public T dequeue(){
		try{
			T tmp = list.remove();
			return tmp;
		}
		catch(NoSuchElementException e){
			System.out.println("Queue leer");
			return null;
		}
	}

	public T peek(){
		return list.peek();
	}
}