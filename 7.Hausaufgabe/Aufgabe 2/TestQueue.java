public class TestQueue{
	public static void main(String[] args) {
		Queue<String> q = new Queue<>();
		q.enqueue("one");
		q.enqueue("two");
		q.enqueue("three");
		q.enqueue("four");
		q.enqueue("five");
		q.enqueue("six");

		System.out.println("First element in queue = "+q.peek());

		for(int i = 0; i < 7; i++) System.out.println("Removed element: "+q.dequeue());

		System.out.println("First element in queue = "+q.peek());
	}
}