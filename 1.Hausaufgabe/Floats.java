public class Floats{
	
	private float[] werte;
	private float kleinstes;

	public Floats(float[] werte){
		this.werte = werte;
	}

	public float kleinere(float x, float y){
		if(Math.abs(x) < Math.abs(y)){
			return x;
		}
		else{
			return y;
		}
	}

	public void setKleinstes(){
		float min = werte[0];
		for(int i = 0; i < werte.length; i++){
			min = kleinere(min, werte[i]);
		}
		kleinstes = min;
	}

	public void ausgabeRekursiv(int index){

		if(index == werte.length-1){
			System.out.println(werte[index]);
			System.out.println("Kleinste element ist: "+kleinstes);
		}

		else{
			System.out.println(werte[index]);
			ausgabeRekursiv(index + 1);
		}
	}
}