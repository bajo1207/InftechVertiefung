public class Auto implements Transportmittel {
	
	private float geschwindigkeit; //In km/h

	public Auto(float geschwindigkeit){
		if(geschwindigkeit <= 100 && geschwindigkeit >= -50f){
		this.geschwindigkeit = geschwindigkeit;
		}
		else{
			this.geschwindigkeit = 0f;
		}
	}
	
	public void beschleunigen(float geschwindigkeit){
		if(this.geschwindigkeit + geschwindigkeit <= 100f && this.geschwindigkeit + geschwindigkeit >= -50f){
			this.geschwindigkeit += geschwindigkeit;
		}
		else{
			System.out.println("Das Auto darf vorwaerts nicht schneller als 100 km/h und rueckwaerts nicht schneller als 50 km/h fahren");
		}
	}

	public float getGeschwindigkeit(){
		return this.geschwindigkeit;
	}
}