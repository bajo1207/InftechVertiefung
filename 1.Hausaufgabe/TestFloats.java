public class TestFloats{
	public static void main(String[] args) {
		Floats[] arr = new Floats[10];
		for(float i = 0.0f; (int)i < arr.length; i++) {
			arr[(int)i] = new Floats(new float[] {1.1f + i, - i * 9.5f, 10 - i + 0.1f*i*i});
		}

		arr[9].setKleinstes();
		arr[9].ausgabeRekursiv(0);
	}
}