public class Mergesort {

    // von aussen zugreifbare Sortier-Methode
    public static void sort(int[] arr) {
    	if(arr.length == 1) return; //Stop condition
    	int mitte = (arr.length)/2; // finds the middle element
    	int[] left = new int[mitte];
    	int[] right = new int[arr.length-mitte];
    	
    	for(int i = 0; i < mitte; i++){ //copies left part of array
    		left[i] = arr[i];
    	}
    	for(int i = 0; i+mitte < arr.length; i++){ //Copies right part of array
    		right[i] = arr[i+mitte];
    	}

    	sort(left); //recursivly devides the left array
    	sort(right); //recursivly devides right array
    	int[] buff = new int[left.length + right.length];
    	merge(buff,left,right);
    	if(buff.length == arr.length){ //Copies the buff array to original array
    		for(int i = 0; i < buff.length; i++){
    			arr[i] = buff[i];
    		}
    	}
    }
    public static void merge(int[] buff, int[] le, int[] ri){
    	int i = 0;
    	int j = 0;
    	int k;
    	for(k = 0; i < le.length && j < ri.length; k++){ //Compares left and right, sets buff element to lowest
    		if(le[i] <= ri[j]){
    			buff[k] = le[i];
    			i++;
    		}
    		else{	
    			buff[k] = ri[j];
    			j++;
    		}
    	}
    	while(i < le.length){ //No more to compare to fills the array with the rest of le
            buff[k] = le[i];
            i++;
            k++;
        }
        while (j < ri.length){ //No more to compare to fills the array with the rest of ri
            buff[k] = ri[j];
            j++;
            k++;
        }
    }

}
